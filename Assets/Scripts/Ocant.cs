﻿using UnityEngine;

public class Ocant : MonoBehaviour {

    public Shader ImageShader;

    public GameObject ImagePlane;
    public GameObject Up;
    public GameObject Down;
    public GameObject Right;
    public GameObject Left;

    public GameObject UpRight;
    public GameObject UpLeft;
    public GameObject DownRight;
    public GameObject DownLeft;

    public float xFactor = 0.028f;
    public float IndentTB = 0.01f;
    public GameObject TextBack;
    public TextMesh Text;
    Vector3 PlusPos;
    Vector3 PlusPosText;

    Texture2D MyTex;

    void Awake()
    {
        ImagePlane.GetComponent<Renderer>().material = new Material(ImageShader);
        PlusPos = TextBack.transform.localPosition - Down.transform.localPosition;
        PlusPosText = Text.transform.localPosition - TextBack.transform.localPosition;
    }

    public void SetTex(Texture2D tex)
    {
        MyTex = tex;
        ImagePlane.GetComponent<Renderer>().material.mainTexture = tex;
    }

    public void SetSize(float Scale, bool SaveProp)
    {
        Scale *= 0.8f;

        transform.localScale = new Vector3(Scale, Scale, Scale);
        SetSize(new Vector2((SaveProp) ? (Mathf.Clamp01(MyTex.width / (float)MyTex.height)) : 1, (SaveProp) ? (Mathf.Clamp01(MyTex.height / (float)MyTex.width)) : 1));
    }

    public void SetSize(Vector2 size)
    {
        ImagePlane.transform.localScale = new Vector3(size.x, size.y, transform.localScale.z);

        Up.transform.localScale = new Vector3(size.x, Up.transform.localScale.y, Up.transform.localScale.z);
        Down.transform.localScale = new Vector3(size.x, Down.transform.localScale.y, Down.transform.localScale.z);

        Right.transform.localScale = new Vector3(Right.transform.localScale.y, Right.transform.localScale.y, size.y);
        Left.transform.localScale = new Vector3(Left.transform.localScale.y, Left.transform.localScale.y, size.y);

        Up.transform.localPosition = new Vector3(Up.transform.localPosition.x, size.y / 2, Up.transform.localPosition.z);
        Down.transform.localPosition = new Vector3(Down.transform.localPosition.x, - size.y / 2, Down.transform.localPosition.z);

        Right.transform.localPosition = new Vector3(size.x / 2, Right.transform.localPosition.y, Right.transform.localPosition.z);
        Left.transform.localPosition = new Vector3(-size.x / 2, Left.transform.localPosition.y, Left.transform.localPosition.z);

        UpRight.transform.localPosition = new Vector3(size.x / 2, size.y / 2, UpRight.transform.localPosition.z);
        UpLeft.transform.localPosition = new Vector3(-size.x / 2, size.y / 2, UpLeft.transform.localPosition.z);

        DownRight.transform.localPosition = new Vector3(size.x / 2, -size.y / 2, DownRight.transform.localPosition.z);
        DownLeft.transform.localPosition = new Vector3(-size.x / 2, -size.y / 2, DownLeft.transform.localPosition.z);

        TextBack.transform.localPosition = Down.transform.localPosition + PlusPos;
        Text.transform.localPosition = TextBack.transform.localPosition + PlusPosText;
    }

    public void SetText(string text)
    {
        Text.text = text;
        TextBack.transform.localScale = new Vector3(xFactor * text.Length + IndentTB*2, TextBack.transform.localScale.y, TextBack.transform.localScale.z);
    }
}
