﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

public static class IOWork{

    public static Texture2D ResTex;

    public static void Init()
    {
        if (!Directory.Exists("Data"))
            Directory.CreateDirectory("Data");
        if (!Directory.Exists("Data/Images"))
            Directory.CreateDirectory("Data/Images");
        if (!Directory.Exists("Data/Images/Main"))
            Directory.CreateDirectory("Data/Images/Main");
        if (!Directory.Exists("Data/Images/Other"))
            Directory.CreateDirectory("Data/Images/Other");
    }

    public static void InitSpawner(Spawner s)
    {
        string Path = "Data/SpawnerSets.txt";
        bool Correct = false;

        if (File.Exists(Path))
        {
            string[] Vals = GetPackages(Encoding.UTF8.GetString(File.ReadAllBytes(Path)));
            try
            {
                s.SaveProportion = GetData(Vals[0]).ToUpper() == "TRUE";
                s.IndentPersent = int.Parse(GetData(Vals[1]));
                s.Up.GetComponent<Renderer>().sharedMaterial.SetTextureScale("_MainTex", new Vector2(float.Parse(GetData(Vals[2])),float.Parse(GetData(Vals[3]))));
                s.Down.GetComponent<Renderer>().sharedMaterial.SetTextureScale("_MainTex", new Vector2(float.Parse(GetData(Vals[4])), float.Parse(GetData(Vals[5]))));
                s.Side.GetComponent<Renderer>().sharedMaterial.SetTextureScale("_MainTex", new Vector2(float.Parse(GetData(Vals[6])), float.Parse(GetData(Vals[7]))));
                Correct = true;
            }
            catch (Exception e) { Debug.Log(e); }
        }
        if (!Correct)
            File.WriteAllBytes(Path, Encoding.UTF8.GetBytes(("SaveProportion: True\r\n" + 
                                                             "IndentPersent: 20\r\n" + 
                                                             "UpTilingX: 3\r\n" +
                                                             "UpTilingY: 3\r\n" +
                                                             "DownTilingX: 20\r\n" +
                                                             "DownTilingY: 20\r\n" +
                                                             "SideTilingX: 1\r\n" +
                                                             "SideTilingY: 1\r\n"
                                                             )));

        Path = "Data/Images/Other/UpTex.png";
        Texture2D CurTex = new Texture2D(0, 0, TextureFormat.ARGB32, true);
        if (File.Exists(Path) && CurTex.LoadImage(File.ReadAllBytes(Path)))
        {
            Texture2D NCurTex = new Texture2D(CurTex.width, CurTex.height, TextureFormat.ARGB32, true);
            NCurTex.SetPixels(CurTex.GetPixels(0, 0, CurTex.width, CurTex.height));
            NCurTex.Apply(true);
            CurTex = NCurTex;

            s.UpTex = CurTex;
        }
        else
            File.WriteAllBytes(Path, ResTex.EncodeToPNG());

        Path = "Data/Images/Other/DownTex.png";
        CurTex = new Texture2D(0, 0, TextureFormat.ARGB32, true);
        
        if (File.Exists(Path) && CurTex.LoadImage(File.ReadAllBytes(Path)))
        {
            Texture2D NCurTex = new Texture2D(CurTex.width, CurTex.height, TextureFormat.ARGB32, true);
            NCurTex.SetPixels(CurTex.GetPixels(0, 0, CurTex.width, CurTex.height));
            NCurTex.Apply(true);
            CurTex = NCurTex;

            s.DownTex = CurTex;
        }
        else
            File.WriteAllBytes(Path, ResTex.EncodeToPNG());

        Path = "Data/Images/Other/SideTex.png";
        CurTex = new Texture2D(0, 0, TextureFormat.ARGB32, true);
        if (File.Exists(Path) && CurTex.LoadImage(File.ReadAllBytes(Path)))
        {
            Texture2D NCurTex = new Texture2D(CurTex.width, CurTex.height, TextureFormat.ARGB32, true);
            NCurTex.SetPixels(CurTex.GetPixels(0, 0, CurTex.width, CurTex.height));
            NCurTex.Apply(true);
            CurTex = NCurTex;

            s.BackTex = CurTex;
        }
        else
            File.WriteAllBytes(Path, ResTex.EncodeToPNG());
    }

    public static void InitMover(Mover m)
    {
        string Path = "Data/MoverSets.txt";
        bool Correct = false;
        if (File.Exists(Path))
        {
            string[] Vals = GetPackages(Encoding.UTF8.GetString(File.ReadAllBytes(Path)));
            try
            {
                m.MoveSpeed = float.Parse(GetData(Vals[0]));
                m.RotSpeed = float.Parse(GetData(Vals[1]));
                m.LookSpeed = float.Parse(GetData(Vals[2]));
                m.MaxUpLook = float.Parse(GetData(Vals[3]));
                m.CamStartForv = float.Parse(GetData(Vals[4]));
                m.Acceleration = GetData(Vals[5]).ToUpper() == "TRUE";
                if (m.Acceleration) 
                    m.AcelerationSpeed = float.Parse(GetData(Vals[6]));
                Correct = true;
            }
            catch (Exception e) { Debug.Log(e); }
        }
        if (!Correct)
            File.WriteAllBytes(Path, Encoding.UTF8.GetBytes(("MoveSpeed: 0.2\r\n" + 
                                                             "RotSpeed: 10\r\n" + 
                                                             "LookSpeed: 20\r\n" + 
                                                             "MaxUpLook: 45\r\n" + 
                                                             "CamStartForv: 0\r\n" + 
                                                             "Acceleration: True\r\n" + 
                                                             "AcelerationSpeed: 50\r\n"
                                                             )));
    }

    public static Texture2D[] GetTextures()
    {
        string[] Names = Directory.GetFiles("Data/Images/Main");
        List<Texture2D> Ans = new List<Texture2D>();
        Texture2D CurTex;
        for (int i = 0; i < Names.Length; i++)
        {
            CurTex = new Texture2D(0, 0, TextureFormat.ARGB32, true);
            if (CurTex.LoadImage(File.ReadAllBytes(Names[i])))
            {
                Texture2D NCurTex = new Texture2D(CurTex.width, CurTex.height, TextureFormat.ARGB32, true);
                NCurTex.SetPixels(CurTex.GetPixels(0, 0, CurTex.width, CurTex.height));
                NCurTex.Apply(true);
                CurTex = NCurTex;

                Names[i] = Names[i].Remove(0, GetIndexOfEnd(Names[i], '/', '\\') + 1);
                CurTex.name = Names[i].Remove(GetIndexOfEnd(Names[i], '.'));
                Ans.Add(CurTex);

            }
        }
        if (Ans.Count < 3)
        {
            if (Ans.Count <= 0)
                Ans.Add(ResTex);
            while (Ans.Count < 3) Ans.Add(Ans[0]);
        }
        return Ans.ToArray();
    }

    static string[] GetPackages(string s, char SepChar = '\r')
    {
        List<string> Ans = new List<string>();
        int Index;

        while (true)
        {
            Index = s.IndexOf(SepChar);

            if (Index < 0) break;
            Ans.Add(s.Remove(Index));
            s = s.Remove(0, Index + 1);
        }

        if (!string.IsNullOrEmpty(s)) Ans.Add(s);

        return Ans.ToArray();
    }

    static string GetData(string s)
    {
        string Ans = "";
        if (!string.IsNullOrEmpty(s))
        {
            int i = 0;
            while(s[i] != ':'){
                i++;
                if (i >= s.Length) break;
            }
            if (i < s.Length)
            {
                i++;
                while (s[i] == ' ')
                {
                    i++;
                    if (i >= s.Length) break;
                }
                while (i < s.Length) { Ans += s[i]; i++; }
            }
        }
        return Ans;
    }

    static int GetIndexOfEnd(string Sourse, params char[] c)
    {
        if (!string.IsNullOrEmpty(Sourse))
        {
            for (int i = Sourse.Length - 1; i > -1; i--)
            {
                for (int j = 0; j < c.Length; j++)
                    if (Sourse[i] == c[j]) return i;
            }
            return -1;
        }
        return -1;
    }
}
