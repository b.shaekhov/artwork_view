﻿using UnityEngine;

public class Mover : MonoBehaviour
{
    public GameObject Camera;
    public CanvasGroup ActivatePanel;

    [HideInInspector]
    public float MoveSpeed = 4;
    [HideInInspector]
    public float RotSpeed = 1;
    [HideInInspector]
    public float LookSpeed = 1;
    [HideInInspector]
    public float MaxUpLook = 60;
    float Rot = 0;
    float RotUp = 0;

    [HideInInspector]
    public bool Acceleration = true;
    [HideInInspector]
    public float AcelerationSpeed = 1;

    float CurSpeed = 0;

    //[HideInInspector]
    public float CamStartForv = 0;

    void Start()
    {
        ActivatePanel.alpha = 0;

        Camera.transform.position += transform.forward * CamStartForv;
        if (!Acceleration)
            CurSpeed = RotSpeed;

        Rot = transform.eulerAngles.y;
    }


    RuleMode CurRule = RuleMode.None;


    public enum RuleMode
    {
        None = 0,
        Left = 1,
        Right = 2,
        Up = 3,
        Down = 4
    }


    public void ActivateMovePanel() 
    {
        ActivatePanel.alpha = 1;
    }
    public void DeactivateMovePanel()
    {
        ActivatePanel.alpha = 0;
    }
    public void SetRuleMode(int newMode) {
        CurRule = (RuleMode)newMode;
    }


    void Update () {
        bool C = false;
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || (CurRule == RuleMode.Right && Input.GetKey(KeyCode.Mouse0))) { CurSpeed += Time.deltaTime * AcelerationSpeed; C = true; }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) || (CurRule == RuleMode.Left && Input.GetKey(KeyCode.Mouse0))) { CurSpeed -= Time.deltaTime * AcelerationSpeed; C = true; }

        if (!C && Acceleration) CurSpeed = 0;
        if (Mathf.Abs(CurSpeed) >= RotSpeed) CurSpeed = RotSpeed * MNO(CurSpeed);

        Rot += Time.deltaTime * CurSpeed;

        if (Input.GetKey(KeyCode.W) || (CurRule == RuleMode.Up && Input.GetKey(KeyCode.Mouse0))) Camera.transform.position += transform.forward * MoveSpeed * Time.deltaTime;
        if (Input.GetKey(KeyCode.S) || (CurRule == RuleMode.Down && Input.GetKey(KeyCode.Mouse0))) Camera.transform.position -= transform.forward * MoveSpeed * Time.deltaTime;

        Camera.transform.position += transform.forward * MoveSpeed * Input.mouseScrollDelta.y * Time.deltaTime;

        if (Input.GetKey(KeyCode.UpArrow)) RotUp -= LookSpeed * Time.deltaTime;
        if (Input.GetKey(KeyCode.DownArrow)) RotUp += LookSpeed * Time.deltaTime;

        if (Mathf.Abs(RotUp) > MaxUpLook) RotUp = MaxUpLook * MNO(RotUp);
        Camera.transform.localEulerAngles = new Vector3(RotUp, Camera.transform.localEulerAngles.y, Camera.transform.localEulerAngles.z);

        if (Mathf.Abs(Rot) >= 360) Rot = Rot % 360;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, Rot ,transform.eulerAngles.z);

        if (Input.GetKey(KeyCode.Escape)) Application.Quit();
	}

    int MNO(float f)
    {
        if (f > 0) return 1;
        if (f < 0) return -1;
        return 0;
    }
}
