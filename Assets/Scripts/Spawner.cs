﻿using UnityEngine;

public class Spawner : MonoBehaviour {

    public Shader ImagesShader;
    
    public GameObject Ocant;
    public GameObject Side;
    
    public GameObject Up;
    public GameObject Down;

    public Texture2D ResTex;
    [HideInInspector]
    public Texture2D BackTex;
    [HideInInspector]
    public Texture2D UpTex;
    [HideInInspector]
    public Texture2D DownTex;

    [HideInInspector]
    public bool SaveProportion = true;

    [HideInInspector]
    public int ImagesCount = 0;
    [HideInInspector]
    public float IndentPersent = 20;

    void Awake()
    {
        IOWork.ResTex = ResTex;
        IOWork.Init();
        IOWork.InitSpawner(this);
        Spawn();
        IOWork.InitMover(GetComponent<Mover>());
    }

    public void Spawn()
    {
        Up.GetComponent<Renderer>().sharedMaterial.mainTexture = UpTex;
        Down.GetComponent<Renderer>().sharedMaterial.mainTexture = DownTex;

        Texture2D[] Texs = IOWork.GetTextures();
        ImagesCount = Texs.Length;

        float Angle = 360f / ImagesCount;
        float CurAngle;
        float OneAngle = (180 * (ImagesCount - 2)) / (float)ImagesCount;
        float Pos = Mathf.Cos((Angle / 2 * Mathf.PI) / 180);
        float Scale = Mathf.Sin((Angle / 2 * Mathf.PI) / 180) * 2;

        float BS = Scale;
        Scale -= Scale * IndentPersent / 100;

        transform.position = new Vector3(0, Scale / 2, 0);
        Up.transform.position = new Vector3(0, Scale, 0);

        GameObject GO;

        for (int i = 0; i < ImagesCount; i++)
        {
            CurAngle = (Angle * i) + Angle / 2;
            CurAngle = (CurAngle * Mathf.PI) / 180;

            GO = Instantiate(Ocant);
            GO.transform.position = new Vector3(Mathf.Cos(CurAngle) * Pos, transform.position.y, Mathf.Sin(CurAngle) * Pos);
            GO.transform.eulerAngles = new Vector3(GO.transform.eulerAngles.x, (OneAngle * i + OneAngle / 2) + (180 * (i & 1)), GO.transform.eulerAngles.z);
            Ocant O = GO.GetComponent<Ocant>();
            O.SetTex(Texs[i]);
            O.SetSize(Scale, SaveProportion);
            O.SetText(Texs[i].name);

            GO = Instantiate(Side);
            GO.transform.eulerAngles = new Vector3(GO.transform.eulerAngles.x, (OneAngle * i + OneAngle / 2) + (180 * (i & 1)), GO.transform.eulerAngles.z);
            GO.transform.localScale = new Vector3(BS+0.001f, BS, 1);
            GO.transform.position = new Vector3(Mathf.Cos(CurAngle) * (Pos + 0.001f), transform.position.y, Mathf.Sin(CurAngle) * (Pos + 0.001f));
            GO.GetComponent<Renderer>().sharedMaterial.mainTexture = BackTex;
        }
    }
}
